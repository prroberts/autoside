# Classifiers
from sklearn.ensemble import *
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from lightgbm import LGBMClassifier
from sklearn.linear_model import Perceptron

# Data Processing
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split as tts
from sklearn.feature_selection import SelectFromModel as sfm

# Data Viz
import matplotlib.pyplot as plt

# General
import numpy as np
import csv
import sys
import os
import argparse
from tabulate import tabulate

# Warnings
import warnings
from sklearn.exceptions import DataConversionWarning

# Dataset
results_sc = r'data/usabe_undoc_nonzero.csv'
results_nosc = r'data/results_undoc_nosc.csv'

class classifierTester:
	# Selected classifiers based on accuracy of initial side channel classification models.
	classifiers = {
        'Perceptron': Perceptron,
		'LightBGM': LGBMClassifier,
		'AdaBoost': AdaBoostClassifier,
		'GradientBoosting': GradientBoostingClassifier,
		#'HistGradientBoosting': HistGradientBoostingClassifier,
		'RandomForest': RandomForestClassifier,
		'ExtraTrees': ExtraTreesClassifier,
		#'MLP': MLPClassifier,
		#'BaggingClassifier': BaggingClassifier
		}

	"""
    Load performance trigger data from a CSV file and prepare it for machine learning.

    Parameters:
    - csv_data (str): The path to the CSV file containing performance trigger data.
    - side_channel (bool): A boolean flag indicating whether the data corresponds to a side channel (True) or not (False).

    Returns:
    - data (numpy.ndarray): A 2D array containing the feature data.
    - target (numpy.ndarray): A 1D array containing the target labels (1.0 for side channel, 0.0 otherwise).
	"""
	def load_perftrigger_from_csv(csv_data, side_channel):
		with open(csv_data) as csv_file:
			data_reader = csv.reader(csv_file)
			feature_names = next(data_reader)[:-1]
			data, target = [], []
			i = 1
			for row in data_reader:
				sys.stdout.flush()
				sys.stdout.write("\rLoading Side Channel Data " + "." * int(i / 100))
				i += 1
				rowdata = [abs(float(diff.split(' ')[0]) - float(diff.split(' ')[1])) if len(diff.split(' ')) > 1 else float(diff) for diff in row]
				data.append(rowdata)
				target.append(1.0) if side_channel else target.append(0.0)

		data, target = np.array(data), np.array(target)
		sys.stdout.flush()
		sys.stdout.write("\rFinished loading sidechannel data.\n")
		return data, target

	"""
	Short wrapper function for reading in side channel data.
	"""
	def get_training_data(sc, nosc):
		data_sc, labels_sc = classifierTester.load_perftrigger_from_csv(sc, 1)
		data_nosc, labels_nosc = classifierTester.load_perftrigger_from_csv(nosc, 0)

		X = np.concatenate((data_sc, data_nosc)).astype(np.float32) 
		y = np.concatenate((labels_sc, labels_nosc)).astype(np.float32) 

		return X, y
	
	"""
	Get the first line of a csv split into a Python list. Used for retrieving the names of the performance triggers.
	"""
	def get_perftrigger_headers(csv: str):
		csv = 'data/results_undoc_nosc.csv'
		f = open(csv)
		return f.read().split('\n')[0].split(',')

	"""
	Test a single classifier for a specified number of iterations. On each iteration, the feature set will be reduced by a specified threshold.

	Parameters:
		- model: The classifier model to be tested.
		- X (array): The feature matrix.
		- y (array): The target variable.
		- test_size (float): The proportion of the dataset to include in the test split.
		- iterations (int): The number of iterations to perform.
		- threshold (float): The threshold for feature selection.
		- random_state (int, optional): Seed for random state to ensure reproducibility (default is 123).

	Returns:
		- scores (list): List of accuracy scores for each iteration.
		- importances (list): List of feature importances for each iteration.
	"""
	def test_features_threshold(model, X, y, test_size, iterations, threshold, random_state=123):
		X_train, X_test, y_train, y_test = tts(X, y, test_size=test_size, random_state=random_state)
		headers = classifierTester.get_perftrigger_headers(results_sc)

		X_train_updating = X_train
		X_test_updating = X_test

		scores = []
		importances = []
		features_removed = []

		for _ in range(iterations):
				# Train model to get metrics on current features
				model.fit(X_train_updating, y_train)
				scores.append(model.score(X_test_updating, y_test))

				# Retrieve the feature importances
				if str(model) == "Perceptron()":
					imps = [abs(val) for val in model.coef_]
				else:
					imps = model.feature_importances_

				# Normalize the feature importances to range (0, 1)
				imps = imps / np.sum(imps)
				importances.append(imps)

				# Turn imps into a python list so we can pythonically grab the index of the smallest feature imp
				imps = imps.tolist()

				# Dumb hack, fix later
				if str(model) == "Perceptron()":
					imps = imps[0]


				# Get indices for importance values that are below the threshold
				
				indices = [index for index, value in enumerate(imps) if value < threshold]
				
				features_removed_step = []

				# Save the labels of the removed features
				headers_to_remove = [headers[value] for value in indices]
				features_removed_step.append(headers_to_remove)

				headers = [value for value in headers if value not in headers_to_remove]
				
				# Delete the columns of the least important features
				X_train_updating = np.delete(X_train_updating, indices, axis=1)
				X_test_updating = np.delete(X_test_updating, indices, axis=1)

				features_removed.append(features_removed_step)

				if not min(X_train_updating.shape):
					print('Removed all features. Ending featureset reduction.')
					break
		
		# If there are features remaining, run the last iteration.
		if min(X_train_updating.shape):
			# Train model to get metrics on current features
			model.fit(X_train_updating, y_train)
			scores.append(model.score(X_test_updating, y_test))

			# Retrieve the feature importances
			if str(model) == "Perceptron()":
				imps = abs(model.coef_)
			else:
				imps = model.feature_importances_

			# Normalize the feature importances to range (0, 1)
			imps = imps / np.sum(imps)
			importances.append(imps)

		return scores, importances, features_removed, headers
	
	"""
	Test a single classifier iteratively and remove the least important feature in the dataset on each iteration.

	Parameters:
		- model: The classifier model to be tested.
		- X (array): The feature matrix.
		- y (array): The target variable.
		- test_size (float): The proportion of the dataset to include in the test split.
		- iterations (int): The number of iterations to perform.
		- step_counter (int): The number of features to remove on each step
		- random_state (int, optional): Seed for random state to ensure reproducibility (default is 123).

	Returns:
		- scores (list): List of accuracy scores for each iteration.
		- features (list): Ordered list of features removed on each iteration.
	"""
	def test_features_step_counter(model, X, y, test_size, iterations, step_counter, random_state=123):
		X_train, X_test, y_train, y_test = tts(X, y, test_size=test_size, random_state=random_state)
		headers = classifierTester.get_perftrigger_headers(results_sc)

		X_train_updating = X_train
		X_test_updating = X_test

		scores = []
		importances = []
		features_removed = []

		for _ in range(min(X_train.shape[1] // step_counter, iterations)):
			# Train model to get metrics on current features
			model.fit(X_train_updating, y_train)
			scores.append(model.score(X_test_updating, y_test))

			# Retrieve the feature importances
			if str(model) == "Perceptron()":
				imps = [abs(val) for val in model.coef_]
			else:
				imps = model.feature_importances_

			# Normalize the feature importances to range (0, 1)
			imps = imps / np.sum(imps)
			importances.append(imps)

			# Turn imps into a python list so we can pythonically grab the index of the smallest feature imp
			imps = imps.tolist()

			# Get the least important values from the feature importances

			# get sorted feat imps
			imps_s = sorted(imps)#[:step_counter]

			indices = []
			for imp in imps_s:
				[indices.append(index) for index, value in enumerate(imps) if value == imp]

			indices = list(set(indices))
			indices = indices[:step_counter]
			features_removed_step = []

			# Save the labels of the removed features
			headers_to_remove = [headers[value] for value in indices]
			features_removed_step.append(headers_to_remove)

			headers = [value for value in headers if value not in headers_to_remove]
			
			# Delete the columns of the least important features
			X_train_updating = np.delete(X_train_updating, indices, axis=1)
			X_test_updating = np.delete(X_test_updating, indices, axis=1)

			features_removed.append(features_removed_step)

		model.fit(X_train_updating, y_train)
		scores.append(model.score(X_test_updating, y_test))

		# Retrieve the feature importances
		if str(model) == "Perceptron()":
			imps = abs(model.coef_)
		else:
			imps = model.feature_importances_

		# Normalize the feature importances to range (0, 1)
		imps = imps / np.sum(imps)
		importances.append(imps)

		return scores, importances, features_removed, headers

	"""
	Main classifier search function.

	Notes
		- Feature importances can vary massively in variances, because they are calculated differently in different models
		- If iterations and threshold are not set, then the function will use iterative feature reduction across the entire feature set.
	
	Parameters:
		models list(str): A list of models to test
		X (numpy-like array): Feature set
		y (numpy-like array): labels
		test_size (float): size of test in train/test split
		iterations (int): number of iterations to use in feature reduction
		threshold (int): Threshold to use for relative feature importance
		random_state (int)

	Returns
		- If the threshold-based feature reduction is used, the function returns the model name, test scores, and feature importances across all rounds of feature reduction.
		- If the iterative feature reduction is used, the function returns the model name, test scores, and the ordered list of features that were removed from the feature set.
		
	"""
	def assess_classifiers(models, X, y, test_size, iterations=None, threshold=None, step_counter=None, random_state=123):
		if threshold == None:
			print('No threshold value given. Using iterative featureset reduction.')
			if step_counter == None:
				print('No step counter value given. Exiting.')
				exit()
		if iterations == None:
			print('No iterations value given. Exiting.')
			exit()

		results = []
		
		# First, determine the models to be used.
		if models == 'all':
			models = classifierTester.classifiers.keys()

		# Loop through each model, and test it for accuracy
		for modelName in models:
			model = classifierTester.classifiers[modelName]
			model = model()
			if threshold == None:
				test_scores, feature_importances, features_removed, kept_features = classifierTester.test_features_step_counter(model, X, y, test_size, iterations, step_counter, random_state=random_state) 
			else:
				test_scores, feature_importances, features_removed, kept_features = classifierTester.test_features_threshold(model, X, y, test_size, iterations, threshold, random_state=random_state)
			results.append([modelName, test_scores, feature_importances, features_removed, kept_features])

		return results

	def plot(X, y, X_name, Y_name, title, filename='out.jpg'):
		plt.plot(X, y)
		plt.xlabel(X_name)
		plt.ylabel(Y_name)
		plt.title(title)
		plt.legend()
		plt.savefig(filename)
		plt.show()
       
if __name__ == "__main__":
	warnings.filterwarnings("ignore")

	# Default values for argument parsing
	defaultModels = 'all'
	defaultThreshold = None
	defaultIterations = None
	defaultStepCounter = None
	defaultTestSize = 0.2

	parser = argparse.ArgumentParser(description='Take in parameters for classifier automation')
	parser.add_argument('--models', metavar='models', nargs='+', default=defaultModels, type=str, help='Select which models to use. Leave empty to use all models, or input a list of models.')
	parser.add_argument('--threshold', metavar='threshold', nargs=1, default=defaultThreshold, type=float, help='Cutoff threshold for feature reduction')
	parser.add_argument('--iterations', metavar='iterations', nargs=1, default=defaultIterations, type=int, help='Number of iterations in feature reduction')
	parser.add_argument('--step_counter', metavar='step_counter', nargs=1, default=defaultStepCounter, type=int, help='Number of step_counter in feature reduction')
	parser.add_argument('--test-size', metavar='test_size', nargs=1, default=defaultTestSize, type=int, help='Test dataset size in train test split')
	parser.add_argument('--normalize', action='store_true',  help='Normalize the dataset.')

	args = parser.parse_args()

	if args.iterations != None:
		args.iterations = args.iterations[0]
	if args.threshold != None:
		args.threshold = args.threshold[0]
	if args.step_counter != None:
		args.step_counter = args.step_counter[0]

	# Get the dataset
	X, y = classifierTester.get_training_data(results_sc, results_nosc)

	# Normalize the performance trigger data.
	if args.normalize:
		X = X / np.sum(X)

	# Test the classifiers
	results = classifierTester.assess_classifiers(args.models, X, y, args.test_size, args.iterations, args.threshold, args.step_counter)

	for classifier in results:
		name = classifier[0]
		scores = classifier[1]
		imps = classifier[2]
		feats_rem = classifier[3]
		feats_kep = classifier[4]

		# Get top ten features from first iteration
		iter_1_imp = imps[0]
		iter_1_kep = feats_kep[0]
        
		print('iter_1_imp.len', len(iter_1_imp))
		print('iter_1_kep.len', len(iter_1_kep))
		print('iter_1_imp', (iter_1_imp))
		print('iter_1_kep', (iter_1_kep))
		exit()

		# Sort the kept features by their importances
		combined = list(zip(iter_1_imp, iter_1_kep))
		sorted_combined = sorted(combined, key=lambda x: x[0])
		sorted_imps, sorted_kep = zip(*sorted_combined)

		# Get the top ten features
		iter_1_ten = sorted_kep[:10]
		print('top ten features from first iteration', iter_1_ten)
		print('top ten feature imps', sorted_imps)

		# Get top ten features from last iteration
		last_imp = imps[-1]
		last_kep = feats_kep[-1]

		# Sort the kept features by their importances
		combined = list(zip(last_imp, last_kep))
		sorted_combined = sorted(combined, key=lambda x: x[0])
		sorted_imps, sorted_last_kep = zip(*sorted_combined)

		# Get the top ten features
		iter_1_ten = sorted_last_kep[:10]
		print('top ten features from last iteration', iter_1_ten)
		print('top ten feature imps', sorted_imps)

		# Get the scores for the first and last iteration
		first = scores[0]
		last = scores[-1]

		print('scores on first it', first)
		print('scores on last it', last)
