# General
import numpy as np
import csv
import sys
import os
import argparse
from tabulate import tabulate

# Import scikit-learn from submodule
sys.path.insert(0,'./scikit-learn')

# Classifiers
from sklearn.ensemble import *
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from classifier_automation import classifierTester
from lightgbm import LGBMClassifier
from sklearn.linear_model import Perceptron
from sklearn.feature_selection import RFE

# Data Processing
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split as tts
from sklearn.feature_selection import SelectFromModel as sfm

# Data Viz
import matplotlib.pyplot as plt

# Warnings
import warnings
from sklearn.exceptions import DataConversionWarning

# Dataset
#results_sc = r'data/usabe_undoc_nonzero.csv'
#results_nosc = r'data/results_undoc_nosc.csv'
#results_sc = r'data/sc_buffer.csv'
#results_nosc = r'data/nosc_buffer.csv'
#results_sc = r'data/results-3-29-24/div_sc_NOCOL.csv'
#results_nosc = r'data/results-3-29-24/div_nosc_NOCOL.csv'
#results_sc = r'data/results-3-29-24/results_add_sc_NOCOL.csv'
#results_nosc = r'data/results-3-29-24/results_add_nosc_NOCOL.csv'

results_sc = "data/results_samemeasure_sc.csv"
results_nosc = "data/results_samemeasure_nosc.csv"

def get_perftrigger_headers():
    csv = 'data/headers.csv'
    #csv = 'data/sc_buffer.csv'
    f = open(csv)
    return f.read().split('\n')[0].split(',')

headers = get_perftrigger_headers()

X, y = classifierTester.get_training_data(results_sc, results_nosc)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=123)

classifiers = {
    'Perceptron': Perceptron,
    'LightBGM': LGBMClassifier,
    'AdaBoost': AdaBoostClassifier,
    'GradientBoosting': GradientBoostingClassifier,
    #'HistGradientBoosting': HistGradientBoostingClassifier,
    'RandomForest': RandomForestClassifier,
    'ExtraTrees': ExtraTreesClassifier,
    #'MLP': MLPClassifier,
    #'BaggingClassifier': BaggingClassifier
    }

result = []

for modelName in classifiers.keys():
    print(modelName)
    model = classifierTester.classifiers[modelName]

    # Get most important features considering all features
    estimator = model()
    estimator.fit(X_train, y_train)

    if modelName == 'Perceptron':
        imps = estimator.coef_[0]
    else:
        imps = estimator.feature_importances_

    top5_indices = sorted(imps.argsort()[-5:][::-1])
    top5_importances = sorted([imps[item] for item in top5_indices])
    top5_features = sorted([headers[item] for item in top5_indices])
    score_all = estimator.score(X_test, y_test)

    print('first 5 indices', top5_indices)
    print('first 5 imps', top5_importances)
    print('first 5 feats', top5_features)

    # Get most important features using RFE
    estimator = model()
    selector = RFE(estimator, n_features_to_select=5, step=1)
    selector = selector.fit(X_train, y_train, X_test, y_test, modelName)
    mask = np.array(selector.support_)

    rfe_top5_indices = np.where(mask == 1)[0]
    print('rfe_top5_indices', rfe_top5_indices)

    if modelName == 'Perceptron':
        imps = selector.estimator_.coef_[0]
    else:
        imps = selector.estimator_.feature_importances_

    rfe_top5_importances = imps
    rfe_top5_features = sorted([headers[item] for item in rfe_top5_indices])
    print('last 5 indices', rfe_top5_indices)
    print('last 5 imps', rfe_top5_importances)
    print('last 5 feats', rfe_top5_features)

    X_test_masked = selector.transform(X_test)
    X_train_masked = selector.transform(X_train)

    score_rfe = selector.score(X_test, y_test)
    print('rfe score', score_rfe)

    # Verify that the score from the RFE is the same as training the estimator with selected features..
    estimator = model()
    estimator.fit(X_train_masked, y_train)
    score_validation = estimator.score(X_test_masked, y_test)

    # Arbitrary tolerance value. The reason we do not test for true equality is due to the entropy built into each model when it is created.
    assert (score_rfe - score_validation) < 0.05

    result.append([modelName,
                  score_all, top5_importances, top5_features,
                  score_rfe, rfe_top5_importances, rfe_top5_features])
    
print(result)
