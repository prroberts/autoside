# Side Channel Classifier Automation

Automation for classifier testing and feature selection for side channel detection datasets.

# Features
 - Automation of testing machine learning classifiers
 - Iterative feature selection