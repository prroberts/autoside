"""
USAGE:

python inputfile.csv

Creates inputfile-subtracted.csv in cwd.
"""

import csv
from sys import argv

filep = argv[1]

if filep is None:
	print('No file provided.')
	exit()

# Add -subtracted to the filename
out = filep.split('.csv')[0] + '-subtracted.csv'

newcsv = ''

with open(filep, 'r') as file:
	csv_reader = csv.reader(file)

	for row in csv_reader:
		newrow = ''
		for cell in row:
			if 'x' not in cell:
				if ' ' in cell:
					vals = [int(val) for val in cell.split(' ')]
					newrow += str(vals[0] - vals[1])
				else:
					print('Cell found without two numbers :(')
					newrow += cell
				newrow += ','
		newcsv += newrow[:-1] + '\n'

f = open(out, 'w')
f.write(newcsv)
f.close()

