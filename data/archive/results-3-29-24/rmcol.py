import csv

def remove_columns(input_file, output_file):
    with open(input_file, 'r') as infile, open(output_file, 'w', newline='') as outfile:
        reader = csv.reader(infile)
        writer = csv.writer(outfile)
        for row in reader:
            # Removing the first two columns
            modified_row = row[2:]
            writer.writerow(modified_row)

# Replace 'input.csv' with the path to your input CSV file
# Replace 'output.csv' with the path where you want to save the modified CSV file
remove_columns('div_sc.csv', 'div_sc_NOCOL.csv')
remove_columns('div_nosc.csv', 'div_nosc_NOCOL.csv')
remove_columns('results_add_sc.csv', 'results_add_sc_NOCOL.csv')
remove_columns('results_add_nosc.csv', 'results_add_nosc_NOCOL.csv')

