import pandas as pd

def getDuplicateColumns(df):
    duplicateColumnNames = set()

    for x in range(df.shape[1]):

        col = df.iloc[:, x]

        for y in range(x + 1, df.shape[1]):

            otherCol = df.iloc[:, y]

            if col.equals(otherCol):
                #duplicateColumnNames.add(df.columns.values[y])
                duplicateColumnNames.add(y)
                duplicateColumnNames.add(x)

    return list(duplicateColumnNames)

def main():
    file_path = 'nosc_buffer.csv'

    try:
        # Read the CSV file into a pandas DataFrame
        df = pd.read_csv(file_path)

        # Check uniqueness for each column
        unique_columns = getDuplicateColumns(df)
        print(unique_columns)

    except FileNotFoundError:
        print(f"File '{file_path}' not found.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")

if __name__ == "__main__":
    main()

