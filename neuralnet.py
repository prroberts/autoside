# General
import numpy as np
import csv
import sys
import os
import argparse
from tabulate import tabulate

# Utility class for loading side channel datasets
from classifier_automation import classifierTester

# Data Processing
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split as tts

# Data Viz
import matplotlib.pyplot as plt

# Warnings
import warnings
from sklearn.exceptions import DataConversionWarning

# PyTorch
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

# Dataset
#results_sc = r'data/usabe_undoc_nonzero.csv'
#results_nosc = r'data/results_undoc_nosc.csv'
results_sc = r'data/sc_buffer.csv'
results_nosc = r'data/nosc_buffer.csv'
#results_sc = r'data/results-3-29-24/div_sc_NOCOL.csv'
#results_nosc = r'data/results-3-29-24/div_nosc_NOCOL.csv'
#results_sc = r'data/results-3-29-24/results_add_sc_NOCOL.csv'
#results_nosc = r'data/results-3-29-24/results_add_nosc_NOCOL.csv'

#results_sc = "data/results_samemeasure_sc.csv"
#results_nosc = "data/results_samemeasure_nosc.csv"


def get_perftrigger_headers():
    #csv = 'data/results_samemeasure_sc.csv'
    csv = 'data/sc_buffer.csv'
    f = open(csv)
    return f.read().split('\n')[0].split(',')

# Define network


class network(nn.Module):
    def __init__(self, input_size):
        super(network, self).__init__()
        self.fc1 = nn.Linear(input_size, 48)
        self.fc2 = nn.Linear(48, 32)
        self.fc3 = nn.Linear(32, 16)
        self.fc4 = nn.Linear(16, 1)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)
        return x


def train_model(X, y):
    X_train, X_test, y_train, y_test = tts(
        X, y, test_size=0.2, random_state=123)

    X_train = torch.tensor(X_train)
    y_train = torch.tensor(y_train)
    X_test = torch.tensor(X_test)
    y_test = torch.tensor(y_test)
    input_size = X_train.shape[1]

    model = network(input_size)

    learning_rate = 0.001
    num_epochs = 2000

    criterion = nn.MSELoss()
    optimizer = optim.SGD(model.parameters(), lr=learning_rate)

    firstloss = None
    for epoch in range(num_epochs):
        outputs = model(X_train)
        outputs = outputs.flatten()
        loss = criterion(outputs, y_train)
        if firstloss is None:
            firstloss = loss
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    with torch.no_grad():
        predicted = model(X_test)
        sumacc = 0
        for pred, true in zip(predicted, y_test):
            acc = (1 - abs(pred - true))
            sumacc += acc
    accuracy = sumacc / len(predicted)
    return accuracy


headers = get_perftrigger_headers()
X, y = classifierTester.get_training_data(results_sc, results_nosc)

remaining_features = X.shape[1]
while remaining_features > 10:
    worst_error = float('inf')
    worst_feature = None
    for i in range(X.shape[1]):
        X_temp = np.delete(X, i, axis=1)
        error = train_model(X_temp, y)
        if error < worst_error:
            worst_error = error
            worst_feature = i
    
    X = np.delete(X, worst_feature, axis=1)
    remaining_features -= 1
    
orig, _ = classifierTester.get_training_data(results_sc, results_nosc)

remaining_indices = [np.where((orig == i).all(axis=0))[0] for i in orig]
remaining_indices = np.concatenate(remaining_indices)
remaining_indices = list(set(list(remaining_indices)))
remaining_headers = [headers[index] for index in remaining_indices]

print(len(remaining_headers))
print(remaining_headers)
